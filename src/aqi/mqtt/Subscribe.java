package aqi.mqtt;

import java.sql.Connection;
import java.sql.Statement;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONObject;

import aqi.models.MySQLConnUtils;

public class Subscribe implements MqttCallback{
	
	MqttClient client;
	
//	public static void main(String[] args) {
//		// TODO Auto-generated method stub
//		new Subscribe().mqttSub();
//	}
	
	public void mqttSub(){
		 try {
		        // Set connection
		        this.setConnection();

		        // Connect
		        client.connect();

		        // Subscribe

		        client.subscribe("paho/test", 0);
		        // Disconnect
		        // client.disconnect();

		      } catch (MqttException e) {
		        e.printStackTrace();
		      }
	}
	
	public void setConnection(){
	    try {
	        // Client
	        client = new MqttClient("tcp://localhost:1883", "mqtt_test_b112358_sub");
	    } catch (MqttException e) {
	        e.printStackTrace();
	    }

//	    // Connection Options
	    MqttConnectOptions options = new MqttConnectOptions();
	    options.setCleanSession(false);

	    // Set the will
	    options.setWill("pahodemo/clienterrors", "CRASHED - CONNECTION NOT CLOSED CLEANLY".getBytes(),2,true);
	   
	    
	    client.setCallback(this);
	}
	@Override
	public void connectionLost(Throwable arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("Message Arrived: " + message + " on tipic: " + topic.getBytes());
		
		Connection connection = MySQLConnUtils.getMySQLConnection();
		Statement statement = connection.createStatement();
		String s = message+"";
		JSONObject json = new JSONObject(s);
		int aqi = json.getInt("aqi");
		int location = json.getInt("location");
		String sql = "insert into aqi_hourly (aqi, location_id)" + " values ("+ aqi +"," + location + ")";
		int rowCount = statement.executeUpdate(sql);
		// In ra số dòng được trèn vào bởi câu lệnh trên.
	      System.out.println("Row Count affected = " + rowCount);
		
		
	}
	
}
