package aqi.models;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectionUtils {

	public static Connection getMyConnection() throws SQLException, ClassNotFoundException {
		return MySQLConnUtils.getMySQLConnection();
	}
	
	public static void main(String[] args) throws SQLException, ClassNotFoundException {
		System.out.println("Get connection ... ");
		 
	      // Lấy ra đối tượng Connection kết nối vào database.
	      Connection connection = ConnectionUtils.getMyConnection();
	 
			Statement statement = connection.createStatement();
			
			String sql = "insert into aqi_hourly (aqi, area_id)" + " values (2,1)";
			
			int rowCount = statement.executeUpdate(sql);
			// In ra số dòng được trèn vào bởi câu lệnh trên.
		    System.out.println("Row Count affected = " + rowCount);
	 
	   
	}
}
